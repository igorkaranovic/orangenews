OrangeNews Application

#### Requrements
- PHP >= 7.2.0
- BCMath PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- libxml PHP Extension


### Installation
Export source and copy .env.example to .env file in root directory, update *APP_URL* and database details

Open terminal, go to source folder and run:
```sh
$ composer install
$ php artisan key:generate
$ php artisan migrate:refresh --seed
```

After executing migration with seeds, you can login with the credentials:<br>
email: admin@orangenews.com<br>
pass: admin 

or create new admin user:
```sh
$ php artisan register:admin
```

Run server

```sh
$ php artisan serve
```

#### Test
Open source folder in terminal and run:
```sh
$ vendor/bin/phpunit
```
