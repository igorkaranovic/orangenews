@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">

                    <h4 class="header-title">Create news category</h4>
                    <div class="row pb-2">
                        <div class="col-md-8"></div>

                        <div class="col-md-4">
                            <a href="{{URL::to('admin/categories/')}}">
                                <button type="button" class="btn btn-outline-secondary pull-right">
                                    List
                                </button>
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::open(['url' => URL::to('admin/categories/create'), 'method' => 'POST']) !!}
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {!! Form::label('name', 'Name') !!}
                                        {!! Form::text('name', null, [
                                            'class' => 'form-control',
                                            'required',
                                            'maxlength' => 100
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('order', 'Order') !!}
                                        {!! Form::number('order', null, [
                                            'class' => 'form-control',
                                            'maxlength' => 5
                                        ]) !!}
                                    </div>
                                </div>
                            </div>

                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            {!! Form::button('Reset', ['class' => 'btn', 'type' => 'reset']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div> <!-- end card body-->
            </div>

        </div>
    </div>

@endsection
