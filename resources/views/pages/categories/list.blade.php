@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">

                    <h4 class="header-title">Categories</h4>
                    <div class="row pb-2">
                        <div class="col-md-8">
                            <p class="text-muted font-14">
                                Create, edit or remove categories.
                            </p>
                        </div>

                        <div class="col-md-4">
                            <a href="{{ URL::to('admin/categories/create') }}">
                                <button type="button" class="btn btn-outline-secondary pull-right">
                                    Add
                                </button>
                            </a>
                        </div>
                    </div>


                    <div class="table-responsive-sm">
                        <table class="table table-bordered mb-0">
                            <thead class="thead-dark">
                            <tr>
                                <th width="20px">id</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th width="20px">Articles</th>
                                <th width="20px">Order</th>
                                <th width="100px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($categories) > 0)
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->id  }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->slug }}</td>
                                        <td class="text-center">{{ $category->articles->count() }}</td>
                                        <td class="text-center">{{ $category->order }}</td>
                                        <td>
                                            <button class="btn dropdown-toggle"
                                                    type="button"
                                                    id="tableActionDropdown"
                                                    data-toggle="dropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false">
                                                <i class="icon-cog"></i>
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="tableActionDropdown">
                                                <a class="dropdown-item"
                                                   href="{{ URL::to('admin/categories/edit/' . $category->id) }}">Edit</a>
                                                <a class="dropdown-item"
                                                   href="{{ URL::to('admin/categories/delete/' . $category->id) }}">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card body-->
            </div>

        </div>
    </div>

@endsection
