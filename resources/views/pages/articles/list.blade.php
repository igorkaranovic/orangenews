@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">

                    <h4 class="header-title">Articles</h4>
                    <div class="row pb-2">
                        <div class="col-md-8">
                            <p class="text-muted font-14">
                                Create, view, edit or remove articles.
                            </p>
                        </div>

                        <div class="col-md-4">
                            <a href="{{ URL::to('admin/articles/create') }}">
                                <button type="button" class="btn btn-outline-secondary pull-right">
                                    Add
                                </button>
                            </a>
                        </div>
                    </div>


                    <div class="table-responsive-sm ">
                        <table class="table table-bordered mb-0">
                            <thead class="thead-dark">
                            <tr>
                                <th>id</th>
                                <th>Headline</th>
                                <th>Category</th>
                                <th>Date</th>
                                <th>Views</th>
                                <th>Active</th>
                                <th width="100px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($articles) > 0)
                                @foreach($articles as $article)
                                    <tr>
                                        <td>{{ $article->id  }}</td>
                                        <td>
                                            <a href="{{URL::to('admin/articles/' . $article->id)}}">
                                                {{ $article->headline }}
                                            </a>
                                        </td>
                                        <td>{{ $article->category->name}}</td>
                                        <td>{{ $article->datetime->format('d.m.Y') }}</td>
                                        <td class="text-center">{{ $article->views }}</td>
                                        <td class="text-center">{{ $article->active ? 'Yes' : 'No' }}</td>
                                        <td>
                                            <button class="btn dropdown-toggle"
                                                    type="button"
                                                    id="tableActionDropdown"
                                                    data-toggle="dropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false">
                                                <i class="icon-cog"></i>
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="tableActionDropdown">
                                                <a class="dropdown-item"
                                                   href="{{ URL::to('admin/articles/' . $article->id) }}">View</a>
                                                <a class="dropdown-item"
                                                   href="{{ URL::to('admin/articles/edit/' . $article->id) }}">Edit</a>
                                                <a class="dropdown-item"
                                                   href="{{ URL::to('admin/articles/delete/' . $article->id) }}">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card body-->
            </div>

        </div>
    </div>

@endsection
