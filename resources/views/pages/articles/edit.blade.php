@extends('layouts.dashboard')

@section ('head-css')
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">

                    <h4 class="header-title">Edit news article</h4>
                    <div class="row pb-2">
                        <div class="col-md-8"></div>

                        <div class="col-md-4">
                            <a href="{{URL::to('admin/articles/')}}">
                                <button type="button" class="btn btn-outline-secondary pull-right">
                                    List
                                </button>
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::open(['url' => URL::to('admin/articles/edit/' . $article->id), 'method' => 'POST']) !!}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('headline', 'Headline') !!}
                                        {!! Form::text('headline', $article->headline, [
                                            'class' => 'form-control',
                                            'required',
                                            'maxlength' => 200
                                        ]) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('description', 'Description') !!}
                                        {!! Form::textarea('description', $article->description, [
                                            'class' => 'form-control',
                                            'rows' => 2,
                                            'required',
                                            'style' => 'resize:none',
                                            'maxlength' => 255
                                        ]) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('category_id', 'Category') !!}
                                        {!! Form::select('category_id', $categories, $article->category_id, [
                                            'placeholder' => 'Please select',
                                            'class' => 'custom-select',
                                            'required'
                                        ]) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('datetime', 'Time') !!}
                                        {{ Form::input('dateTime-local', 'datetime', $article->datetime->format('Y-m-d\TH:i'), ['class' => 'form-control', 'required']) }}
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group form-check" style="padding-top: 40px;">
                                        {!! Form::checkbox('active', '1', boolval($article->active), ['id' => 'active', 'class' => 'form-check-input']) !!}
                                        {!! Form::label('active', 'Active') !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-5 mb-5">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('content', 'Content') !!}
                                        {!! Form::textarea('content', $article->content, [
                                            'class' => 'form-control',
                                            'rows' => 10,
                                            'required'
                                        ]) !!}
                                    </div>
                                </div>
                            </div>

                            <script>
                                CKEDITOR.replace( 'content' );
                            </script>

                            {!! Form::hidden('id', $article->id) !!}

                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            {!! Form::button('Reset', ['class' => 'btn', 'type' => 'reset']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div> <!-- end card body-->
            </div>

        </div>
    </div>

@endsection
