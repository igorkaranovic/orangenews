@extends('layouts.dashboard')
@section('head-css')
    <style type="text/css">
        body {
            background: #eeeeee;
            height: auto;
            min-height: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="pr-3">
                <a href="{{ URL::to('admin/articles/delete/' . $article->id) }}">
                    <button type="button" class="btn btn-outline-danger pull-right ml-2">
                        Delete
                    </button>
                </a>

                <a href="{{ URL::to('admin/articles/edit/' . $article->id) }}">
                    <button type="button" class="btn btn-outline-secondary pull-right ml-2">
                        Edit
                    </button>
                </a>

                <a href="{{ URL::to('admin/articles/') }}">
                    <button type="button" class="btn btn-outline-secondary pull-right ml-2">
                        List articles
                    </button>
                </a>
            </div>
        </div>

        <div class="col-md-12">
            <main role="main" class="container">

                <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded shadow-sm"
                     style="background: rgb(0, 123, 255);">

                    <div class="row" style="width: 100%;">
                        <div class="col-md-10">
                            <div class="lh-100">
                                <h2 class="mb-0 text-white lh-100">
                                    {{ $article->headline }}
                                </h2>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5>
                                {{ $article->description}}
                            </h5>
                        </div>

                    </div>

                </div>

                <div class="my-3 p-3 bg-white rounded shadow-sm">
                    <div class="row mb-3">
                        <div class="col-md-12 pull-right" style="text-align: right">
                            <span>{{ $article->category->name }} </span>
                            <span>| {{ $article->datetime->format('m.d.Y') }} </span>
                        </div>
                    </div>

                    {!! $article->content  !!}
                </div>

            </main>
        </div>

    </div>

@endsection
