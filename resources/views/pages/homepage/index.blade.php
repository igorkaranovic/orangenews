@extends('layouts.front')

@section('content')
    <div class="row mb-2">
        @if (empty($featured) && $articles->count() == 0)
            <div class="col-12">
                <p class="text-center pt-5">No articles found...</p>
            </div>
        @endif

        @foreach($articles as $article)
            <div class="col-md-12">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <h3 class="mb-0">
                            <a href="{{ URL::to('news/' . $article->id) }}" style="color:#000;">
                                {{ $article->headline }}
                            </a>
                        </h3>
                        <div class="mb-1 text-muted">
                            <strong class="d-inline-block mb-2">
                                {{ $article->category->name  }}
                            </strong> | {{ $article->datetime->format('M d  H:i')  }}
                        </div>
                        <p class="card-text mb-auto">{{ $article->description }}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
