@extends('layouts.front')

@section('content')
    <div class="row news-single">
        <div class="col-12">
            <div class="jumbotron p-4 p-md-5 text-white bg-dark featured-news">
                <div class="col-md-12 px-0">
                    <h1 class="display-5">{{ $article->headline }}</h1>
                </div>
            </div>
        </div>

        <div class="col-md-12 news-main">
            <div class="news-post">
                <h5 class="">{{ $article->description }}</h5>
                <p class="news-post-meta">
                    <a href="{{  URL::to('cat/' . $article->category->slug) }}">
                        {{ strtoupper($article->category->name) }}
                    </a> |
                    {{ $article->datetime->format('F d, Y  H:i') }}
                </p>

                <div class="news-single-content">
                    {!! $article->content !!}
                </div>
            </div>
        </div><!-- /.row -->
@endsection
