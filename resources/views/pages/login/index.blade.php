@extends('layouts/clear')

@section('title')Login @endsection

@section('content')
    {!! Form::open(['url' => URL::to('login'), 'method' => 'POST', 'class'=>'form-login']) !!}
    @csrf

    <h1 class="h3 mb-3 font-weight-normal">Log in</h1>
    <p>Please enter your credentials</p>

    @if ($errors->any())
        <div class="alert">
            <ul class="list-group">
                @foreach ($errors->all() as $error)
                    <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::label('email', 'E-Mail Address', ['class' => 'sr-only']) !!}
    {!! Form::email('email', $value = null, $attributes = ['class' => 'form-control', 'placeholder' => 'Email address', 'required', 'autofocus']); !!}

    {!! Form::label('password', 'Password', ['class' => 'sr-only']) !!}
    {!! Form::password('password', $attributes = ['class' => 'form-control', 'placeholder' => 'Password', 'required']); !!}

    {!! Form::submit('Log in', ['class' => 'btn btn-lg btn-primary btn-block']) !!}
    <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
    {!! Form::close() !!}
@endsection
