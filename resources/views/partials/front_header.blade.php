<?php
    $category_slug = $category_slug ?? '';
?>
<header class="news-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="col-4 offset-4 text-center">
            <a class="news-header-logo text-dark" href="{{ URL::to('/') }}">{{ env('APP_NAME') }}</a>
        </div>
        @include('partials.front_header_login')
    </div>
</header>

<div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-between">
        @foreach($categories as $category)
                <a class="{{$category->slug === $category_slug ? 'font-weight-bold' : ''}} p-2 text-muted"
                   href="{{ URL::to( 'cat/' . $category->slug ) }}">
                    {{ $category->name }}
                </a>
        @endforeach
    </nav>
</div>

@include('partials.front_featured')

<div class="row">
    <div class="col-12">
        @include('partials.error_messages')
    </div>
</div>
