@if (isset($featured))
<div class="jumbotron p-4 p-md-5 text-dark bg-dark featured-news">
    <div class="col-md-8 px-0">
        <h1 class="display-5">
            <a href="{{ URL::to('news/' . $featured->id) }}">{{ $featured->headline }}</a>
        </h1>
        <p class="lead my-3">{{ $featured->description }}</p>
    </div>
</div>
@endif
