<div class="col-4 d-flex justify-content-end align-items-center">
    @if(Auth::user())
        <a class="btn btn-sm btn-outline-secondary" href="{{ URL::to('admin') }}">Admin panel</a>
        <a class="btn btn-sm btn-outline-secondary ml-2" href="{{ URL::to('logout') }}">Log out</a>
    @else
        <a class="btn btn-sm btn-outline-secondary" href="{{ URL::to('login') }}">Log in</a>
    @endif
</div>
