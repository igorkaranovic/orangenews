<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currentTime = Carbon::now();
        $data = [
            [
                'name' => 'administrator',
                'email' => 'admin@orangenews.com',
                'password' => bcrypt('admin'),
                'email_verified_at' => $currentTime,
                'is_admin' => 1,
                'created_at' => $currentTime,
            ],
            [
                'name' => 'editor',
                'email' => 'username@host.com',
                'password' => bcrypt('editor'),
                'email_verified_at' => $currentTime,
                'is_admin' => 0,
                'created_at' => $currentTime,
            ]
        ];

        DB::table('users')->insert($data);
    }
}
