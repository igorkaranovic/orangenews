<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currentTime = Carbon::now();
        $data = [
            [
                'name' => 'Uncategorized',
                'slug' => 'uncategorized',
                'order' => 1,
                'created_at' => $currentTime,
            ],
            [
                'name' => 'World',
                'slug' => 'world',
                'order' => 2,
                'created_at' => $currentTime,
            ],
            [
                'name' => 'Business',
                'slug' => 'business',
                'order' => 4,
                'created_at' => $currentTime,
            ],
            [
                'name' => 'Tech',
                'slug' => 'tech',
                'order' => 3,
                'created_at' => $currentTime,
            ],
            [
                'name' => 'Sport',
                'slug' => 'sport',
                'order' => 5,
                'created_at' => $currentTime,
            ],
            [
                'name' => 'Entertainment & Arts',
                'slug' => 'entertainment-arts',
                'order' => 6,
                'created_at' => $currentTime,
            ],
            [
                'name' => 'Health',
                'slug' => 'health',
                'order' => 7,
                'created_at' => $currentTime,
            ]
        ];

        DB::table('categories')->insert($data);
    }
}

