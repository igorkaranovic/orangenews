<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        $currentTime = Carbon::now();
        $data = [
            [
                'headline' => 'Coronavirus: Italy to close all schools as deaths rise',
                'description' => 'Italy has confirmed that it will shut all schools from Thursday for 10 days as it battles to contain the coronavirus outbreak.',
                'content' => '<p>A total of 107 people have now been killed by the coronavirus in Italy, which has the most serious outbreak in Europe. PM Giuseppe Conte said the health service risked being overwhelmed. Most of the more than 3,000 cases are in the north but others have been confirmed in 19 of Italy&#39;s 20 regions.</p>
<p>Globally about 3,200 people have died and more than 90,000 have been infected, the vast majority in China, where the virus emerged late last year.</p>
<p>The World Health Organization has so far stopped short of declaring a pandemic - an epidemic spreading across the world through local transmission - but on Wednesday Germany&#39;s health minister said the coronavirus now met the definition.</p>
<p>&quot;The situation is changing very quickly... What&#39;s clear is that we have not yet reached the peak of the outbreak,&quot; Jens Spahn said. Confirmed cases have been reported in 81 countries, with Italy, Iran and South Korea emerging as hotspots outside China.</p>',
                'datetime' => $this->randomDate(),
                'category_id' => 2,
                'creator_id' => 1,
                'active' => 1,
                'created_at' => $currentTime,
            ],
            [
                'headline' => 'Climate change boosted Australia bushfire risk by at least 30%',
                'description' => 'Scientists have published the first assessment quantifying the role of climate change in the recent Australian bushfires.',
                'content' => "<p>Global warming boosted the risk of the hot, dry weather that&#39;s likely to cause bushfires by at least 30%, they say.</p>
<p>But the study suggests the figure is likely to be much greater.</p>
<p>It says that if global temperatures rise by 2C, as seems likely, such conditions would occur at least four times more often.</p>
<p>The analysis has been carried out by the World Weather Attribution consortium.</p>
<p>Co-author Geert Jan van Oldenborgh of the Royal Netherlands Meteorological Institute in De Bilt, The Netherlands, told the BBC even the study&#39;s very conservative estimates were troubling.</p>
<p>&quot;Last year the fire prevention system in Australia, which is extremely well prepared for bushfires, was straining. It was at the limits of what it could handle, with volunteers working for weeks on end,&quot; said Prof van Oldenborgh.</p>
<p>&quot;As the world warms, these events will become more likely and more common. And it&#39;s not something that we are ready for.&quot;</p>
<p>During the 2019-2020 fire season in Australia, record-breaking temperatures and months of severe drought fuelled&nbsp;<a href=\"https://www.bbc.co.uk/news/world-australia-50951043\">a series of massive bushfires across the country.</a></p>
<p>At least 33 people were killed and more than 11 million hectares (110,000 sq km or 27.2 million acres) of bush, forest and parks across Australia burned.</p>
<p>Although it makes sense that human-induced global warming is likely to have led to more bushfires, assigning a figure to that increased risk is complex.</p>
<p>That is because other factors not directly related to climate change may also play a significant role. These include increased water use making the land drier, urban heating effects or unknown local factors.</p>
<p>Nevertheless, Prof Jan van Oldenborgh and 17 fellow climate scientists from six countries gave it their best shot. &quot;It was by far the most complex study we have undertaken,&quot; he told the BBC.</p>                ",
                'datetime' => $this->randomDate(),
                'category_id' => 2,
                'creator_id' => 1,
                'active' => 1,
                'created_at' => $currentTime,
            ],
            [
                'headline' => 'Super Tuesday: Biden seals comeback with string of victories',
                'description' => "Joe Biden has won nine of the 14 states that voted to pick a Democratic White House candidate on Super Tuesday, a remarkable rebound for his campaign.",
                'content' => '<p>The former US vice-president overturned predictions to narrowly take the key state of Texas from his main challenger, Bernie Sanders.</p>
<p>However, Mr Sanders is projected to win California - the biggest prize of the night - as well as three other states.</p>
<p>They lead the race to face Republican President Donald Trump in November.</p>
<ul>
	<li>Super Tuesday: The winners and losers</li>
	<li>How the big night unfolded</li>
	<li>Super Tuesday results in full</li>
</ul>
<p>Former New York mayor Michael Bloomberg spent more than $500m (&pound;390m) of his own money on his campaign, but did not win a single state. And Senator Elizabeth Warren, once the frontrunner in the race, suffered a humiliating defeat to Mr Biden in her home state of Massachusetts.</p>
<p>Super Tuesday awards more than 1,300 of the 1,991 delegates needed to clinch the Democratic White House nomination in July.</p>
<p>As things stand, Mr Biden has 402 delegates and Mr Sanders 314. However, results from California, which has 415 delegates, could affect the current standing.</p>
<p><img alt="map of Biden\'s wins" src="https://ichef.bbci.co.uk/news/624/cpsprodpb/9992/production/_111141393_st_results_map-nc.png" style="height:699px; width:976px" /></p>
<h2>How important is Biden&#39;s performance?</h2>
<p>Only last month, Mr Biden&#39;s campaign was all but written off by some observers after he finished a poor fourth in the Iowa caucuses and fifth in the New Hampshire primary.</p>
<p>But on Tuesday, Mr Biden won Texas - the second biggest state with 228 delegates - along with Massachusetts, Minnesota, Oklahoma, Arkansas, Alabama, Tennessee, North Carolina and Virginia.</p>
<p>Maine, which has 24 delegates, is also yet to declare but partial results put Mr Biden slightly ahead of Mr Sanders.</p>
<p>Virginia and North Carolina are crucial because they are key swing states in the 2020 election.</p>
<ul>
	<li>What are primaries and how do they work?</li>
</ul>
<p>Exit polls across the board suggested Mr Biden - who was vice-president to Barack Obama - attracted large majorities of African-American voters, a crucial bloc for the Democratic party.</p>
<p>Mr Biden, 77, also appears to have won among the type of suburban voters who pollsters say have been turning away from the current US president.</p>
<p>&quot;We are very much alive,&quot; Mr Biden told a crowd in Los Angeles. &quot;Make no mistake about it, this campaign will send Donald Trump packing.&quot;</p>
',
                'datetime' => $this->randomDate(),
                'category_id' => 2,
                'creator_id' => 1,
                'active' => 1,
                'created_at' => $currentTime,
            ],
            [
                'headline' => 'Greta Thunberg brands EU\'s new climate law \'surrender\'',
                'description' => "Swedish teen activist Greta Thunberg has rebuked the EU's plan for tackling climate change, telling MEPs it amounts to \"surrender\".",
                'content' => '<p>Ms Thunberg spoke in Brussels on Wednesday as the EU unveiled a proposed law for reducing carbon emissions.</p>
<p>If passed, the law would make it a legal requirement for the EU to be carbon neutral by 2050.</p>
<p>EU Commission President Ursula von der Leyen hailed the law as the &quot;heart of the European Green Deal&quot;.</p>
<p>But 17-year-old Ms Thunberg dismissed the law as &quot;empty words&quot;, accusing the EU of &quot;pretending&quot; to be a leader on climate change.</p>
<p>&quot;When your house is on fire, you don&#39;t wait a few more years to start putting it out. And yet this is what the Commission is proposing today,&quot; Thunberg told the European Parliament&#39;s environment committee.</p>
<p>She said the law, which would give the EU Commission more powers to set tougher carbon reduction goals, did not go far enough.</p>
<p><img alt="Greta Thunberg speaks a meeting at the European Parliament in Brussels on 4 March, 2020" src="https://ichef.bbci.co.uk/news/624/cpsprodpb/4775/production/_111139281_060429441-1.jpg" style="height:549px; width:976px" />Image copyrightGETTY IMAGES</p>
<p>Image captionMs Thunberg is well-known for criticising politicians on climate change</p>
<p>The law, Ms Thunberg said, was an admission that the EU was &quot;giving up&quot; on the Paris agreement -&nbsp;<a href="https://unfccc.int/process/the-paris-agreement/status-of-ratification">a deal which committed 197 nations to greenhouse gas reductions</a>.</p>
<p>&quot;This climate law is surrender. Nature doesn&#39;t bargain, and you cannot make deals with physics,&quot; the activist said.</p>
<p>She said its Green Deal package of measures would give the world &quot;much less than a 50% chance&quot; to limit global warming to 1.5C.</p>
<p>Countries signed up to the Paris climate deal have agreed to &quot;endeavour to limit&quot; global temperatures below 1.5C.</p>
<p><img alt="Presentational grey line" src="https://ichef.bbci.co.uk/news/624/cpsprodpb/1226D/production/_105894347_grey_line-nc.png" style="height:2px; width:640px" /></p>
<h2>What is the EU&#39;s Green Deal?</h2>
<p>The European Green Deal includes:</p>
<ul>
	<li>A &euro;100bn (&pound;86m) Just Transition Mechanism to help countries still heavily dependent on fossil fuels and &quot;carbon-intensive processes&quot; to move to renewable energy sources</li>
	<li>Proposals to reduce greenhouse gas emissions to 50% of 1990 levels or even lower by 2030 - instead of the current target of a 40% cut</li>
	<li>A law that would set the EU &quot;on an irreversible path to climate neutrality&quot; by 2050</li>
	<li>A plan to promote a more circular economy - a system designed to eliminate waste - that would address more sustainable products as well as a &quot;farm to fork&quot; strategy to improve the sustainability of food production and distribution</li>
</ul>
',
                'datetime' => $this->randomDate(),
                'category_id' => 2,
                'creator_id' => 1,
                'active' => 1,
                'created_at' => $currentTime,
            ],
            [
                'headline' => 'Lego: Toy retailer bets on shops despite toy market downturn',
                'description' => "Danish toy retailer Lego is betting big on physical stores despite falling demand in the wider toy market.",
                'content' => '<p>It plans to open 150 branded shops around the world in 2020, having opened the same amount last year.</p>
<p>Lego&#39;s chief executive, Niels Christiansen, told the BBC that he &quot;wanted people to get their hands on bricks and be a part of the brand&quot;.</p>
<p>The company estimates that the global toy market shrank by 3% in 2019.</p>
<p>Mr Christiansen said: &quot;Some of the changes in the retail landscape have put toy retailers under pressure... But we see the great power of people getting their hands on bricks.&quot;</p>
<p>The firm, which has 570 stores worldwide, has traditionally sold its goods through third-party retailers.</p>
<p>Its decision to open more High Street sites comes despite a spate of retail closures.</p>
<p>The biggest casualty in the toy industry has been Toys R Us, which collapsed into administration in the UK in 2018.</p>',
                'datetime' => $this->randomDate(),
                'category_id' => 3,
                'creator_id' => 1,
                'active' => 1,
                'created_at' => $currentTime,
            ],
            [
                'headline' => 'Tesla downgraded Model 3 chip in China thanks to coronavirus',
                'description' => "Tesla has revealed that it installed older, slower processors in new cars because of supply chain issues caused by the coronavirus outbreak.",
                'content' => '<p>Tesla has revealed that it installed older, slower processors in new cars because of supply chain issues caused by the coronavirus outbreak.</p>
<p>The firm had received complaints from owners of new Model 3 vehicles in China who found their cars&#39; computers used the older chip.</p>
<p>In a statement, Tesla said it would upgrade the hardware free of charge, when supplies allowed.</p>
<p>The chip in question is used by the car&#39;s Autopilot system.</p>
<p>Autopilot provides semi-automatic driving, for example by taking control of steering, accelerating and braking.</p>
<p>Owners of new Model 3 vehicles produced in China expected to find the 3.0 processor in their cars - but found the 2.5 version instead.</p>
<p>The 3.0 chip has been included in new Tesla cars since April last year.</p>
<p>It processes images 21 times faster than the 2.5 version.</p>
<p>The first Tesla Model 3 vehicles made in China were delivered to customers on 7 January.</p>
<p>However, due to the coronavirus outbreak, Tesla&#39;s factory in Shanghai closed at the end of January. It reopened on 10 February.</p>
',
                'datetime' => $this->randomDate(),
                'category_id' => 3,
                'creator_id' => 1,
                'active' => 1,
                'created_at' => $currentTime,
            ],
            [
                'headline' => 'Boots halts Advantage Card payments after cyber-attack',
                'description' => "Boots has suspended payments using loyalty points in shops and online after attempts to break into customers' accounts using stolen passwords.",
                'content' => '<p>Customers will not be able to use Boots Advantage Card points to pay for products while the issue is dealt with.</p>
<p>Boots said none of its own systems were compromised, but attackers had tried to access accounts using reused passwords from other sites.</p>
<p>It comes days after a similar issue hit 600,000 Tesco Clubcard holders.</p>
<p>A spokeswoman for Boots told the BBC the issue affected less than 1% of the company&#39;s 14.4 million active Advantage Cards - fewer than 150,000 people.</p>
<p>But it could not give an exact number as the company was still dealing with the problem.</p>
<p>No credit card information had been accessed, they said.</p>
<p>Suspending payments using points removed the risk of hackers stealing the points to spend themselves, the spokeswoman said.</p>
<p>Customers can still earn points when making purchases, and Boots hopes to have point payments back up as soon as possible.</p>
',
                'datetime' => $this->randomDate(),
                'category_id' => 4,
                'creator_id' => 1,
                'active' => 1,
                'created_at' => $currentTime,
            ],
            [
                'headline' => 'Facebook \'rethinks\' plans for Libra cryptocurrency',
                'description' => "Facebook is reportedly rethinking its plans for its own digital currency after resistance from regulators.",
                'content' => '<p>Facebook is reportedly rethinking its plans for its own digital currency after resistance from regulators.</p>
<p>It is now considering a system with digital versions of established currencies, including the dollar and the Euro, according to Bloomberg and tech site The Information.</p>
<p>The Libra Association, which Facebook founded to create the currency, will continue its work, the reports said.</p>
<p>The plan will include Libra, the company said in response.</p>
<p>The social network&#39;s digital wallet is now expected to launch this autumn, several months later than initially planned, according to the reports.</p>
<p>Of earlier reports that it might drop Libra itself, the firm said: &quot;Facebook remains fully committed to the project.&quot;</p>
<p>Facebook announced in June last year that it would launch the Libra digital currency, with a goal of making payments easier and cheaper.</p>
<p>Its partners in the Libra Association include Lyft, Spotify, Shopify, but several other high-profile members such as Visa left after the idea was criticised by authorities.</p>
<p>Dante Disparte, head of Policy and Communications at the Libra Association said: &quot;The Libra Association has not altered its goal of building a regulatory compliant global payment network, and the basic design principles that support that goal have not been changed nor has the potential for this network to foster future innovation.&quot;</p>a
<p>In October, the world&#39;s biggest economies warned cryptocurrencies such as Libra pose a risk to the global financial system.</p>
',
                'datetime' => $this->randomDate(),
                'category_id' => 4,
                'creator_id' => 1,
                'active' => 1,
                'created_at' => $currentTime,
            ],
            [
                'headline' => 'Release of James Bond film No Time To Die delayed amid coronavirus fears',
                'description' => "The release of the new James Bond film has been put back by seven months as coronavirus continues to spread.",
                'content' => '<p>The producers said they had moved the release of No Time To Die from April to November after &quot;careful consideration and thorough evaluation of the global theatrical marketplace&quot;.</p>
<p>The announcement comes days after the founders of two 007 fan sites&nbsp;<a href="https://www.bbc.co.uk/news/entertainment-arts-51720354">called on the film studios</a>&nbsp;to delay its release.</p>
<p>It will now come out in the UK on 12 November, and in the US on 25 November.</p>
<p>No Time To Die, which is due to be Daniel Craig&#39;s final appearance as the British secret service agent, had been due for release on 3 April.</p>',
                'datetime' => $this->randomDate(),
                'category_id' => 6,
                'creator_id' => 1,
                'active' => 1,
                'created_at' => $currentTime,
            ],
            [
                'headline' => 'Amy Winehouse honoured on Camden\'s Music Walk of Fame',
                'description' => "Fans chanted Amy Winehouse's name as a stone bearing her name was unveiled on the Music Walk of Fame in Camden, north London.",
                'content' => '<p>The singer&#39;s parents, Mitch and Janis, attended the ceremony, calling it a &quot;tremendous accolade and a privilege&quot;.</p>
<p>Camden was the star&#39;s main stomping ground and fans regularly pay their respects outside her former house.</p>
<p>&quot;She loved Camden and now she&#39;s indelibly part of the streets,&quot; Mitch told BBC 6 Music&#39;s Matt Everitt.</p>
<p>&quot;It&#39;s a wonderful tribute to her - but, on the other hand, it reminds us once again that she&#39;s not here.&quot;</p>
<p>Winehouse achieved international acclaim in 2006 with her second album Back to Black, winning multiple Brit and Grammy awards.</p>',
                'datetime' => $this->randomDate(),
                'category_id' => 6,
                'creator_id' => 1,
                'active' => 1,
                'created_at' => $currentTime,
            ],
        ];

        DB::table('articles')->insert($data);
    }

    private function randomDate() : DateTime {
        return Carbon::now()->subMinutes(rand(1, 14440));
    }
}
