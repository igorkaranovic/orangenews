<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Category::class, function (Faker $faker) {
    $name = $faker->name;

    return [
        'name' => $name,
        'slug' => Str::slug($name),
        'order' => $faker->unique()->numberBetween(1,50)
    ];
});
