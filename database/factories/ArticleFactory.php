<?php

/** @var Factory $factory */

use App\Article;
use App\Category;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Article::class, function (Faker $faker) {
    $user = factory(User::class)->create();
    $category = factory(Category::class)->create();

    return [
        'headline' => $faker->sentence(5),
        'description' => $faker->sentence(10),
        'content' => $faker->text(),
        'active' => 1,
        'datetime' => now(),

        'creator_id' => $user->id,
        'category_id' => $category->id
    ];
});
