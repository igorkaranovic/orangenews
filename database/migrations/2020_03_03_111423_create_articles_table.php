<?php

use Carbon\Carbon as Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('creator_id');
            $table->unsignedBigInteger('category_id');

            $table->string('headline', 200);
            $table->string('description');
            $table->text('content');
            $table->dateTime('datetime')->default(Carbon::now());

            $table->unsignedInteger('views')->default(0);
            $table->unsignedTinyInteger('active')->default(1);
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
