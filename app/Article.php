<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'creator_id', 'category_id', 'headline', 'content', 'description', 'datetime', 'active'
    ];

    protected $casts = [
        'active' => 'boolean',
        'views' => 'integer',
        'datetime' => 'datetime',
    ];

    /**
     * Get active articles, ordered by datetime field
     * @param int $skip
     * @return object
     */
    public static function getActive($skip = 0) {
        return self::where('active', 1)
            ->orderBy('datetime', 'desc')
            ->offset($skip)
            ->take(1000)
            ->get();
    }

    /**
     * Get all articles
     * @param string $order_by
     * @param string $order
     * @return Article
     */
    public static function getAll($order_by = 'id', $order = 'DESC') {
        return self::orderBy($order_by, $order)->get();
    }

    /**
     * Get latest active article
     */
    public static function getLatest() {
        return self::where('active', 1)
            ->orderBy('datetime', 'desc')
            ->first();
    }

    public function user() {
        return $this->belongsTo('App\User', 'creator_id', 'id');
    }

    public function category() {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }
}
