<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'slug', 'order'
    ];

    protected $casts = [
        'order' => 'integer'
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            return self::beforeDelete($model);
        });
    }

    public function articles() {
        return $this->hasMany('App\Article', 'category_id', 'id');
    }

    public function active_articles() {
        return $this->hasMany('App\Article', 'category_id', 'id')->get();
    }

    public static function getBySlug(string $slug) {
        return self::query()->where('slug', $slug)->firstOrFail();
    }

    /**
     * Get all categories except uncategorized
     *
     * @return Collection
     */
    public static function getPublic() {
        return self::query()
            ->where('name', '!=', 'Uncategorized')
            ->orderBy('order', 'asc')
            ->get();
    }

    /**
     * Get all categories
     *
     * @return Collection
     */
    public static function getAll() {
        return self::query()
            ->orderBy('order', 'asc')
            ->get();
    }

    /**
     * Get list of categories
     *
     * @return array
     */
    public static function getList() : array
    {
        $list = [];
        $categories = self::getAll();

        foreach ($categories as $category) {
            $list[$category->id] = $category->name;
        }

        return $list;
    }

    public static function getDefault() {
        return self::query()
            ->where('name', '=', 'Uncategorized')
            ->first();
    }

    public static function getHighestOrderNumber() : int
    {
        $result = self::query()
            ->select('order')
            ->orderBy('order', 'desc')
            ->first();

        return ($result) ? $result->order + 1 : 1;
    }

    /**
     * Before category delete, move all articles to default category
     *
     * @param $model
     * @return bool
     */
    private static function beforeDelete($model) : bool {
        if ($model->name === 'Uncategorized')
            return false;

        $default = self::getDefault();
        foreach ($model->articles as $article) {
            $article->category_id = $default->id;
            $article->save();
        }

        return true;
    }
}
