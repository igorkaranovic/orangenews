<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginFormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

class SessionController extends Controller
{

    /**
     * Load login page
     */
    public function login() {
        if (Auth::user()) {
            return redirect('admin');
        }

        return view('pages/login/index');
    }

    /**
     * Authenticate and redirect user to admin panel
     *
     * @param LoginFormRequest $request
     * @param MessageBag $messageBag
     * @return RedirectResponse|Redirector
     */
    public function authenticate(LoginFormRequest $request, MessageBag $messageBag) {
        if (Auth::user())
            return redirect('/admin');

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended('/admin');
        }

        $messageBag->add('credentials', 'Incorrect email or password');
        return redirect('login')->withErrors($messageBag);
    }

    /** Logout user and redirect to home page
     *
     * @return RedirectResponse|Redirector
     */
    public function logout() {
        if ($user = Auth::user())
            Auth::logout();

        return redirect('/');
    }
}
