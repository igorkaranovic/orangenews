<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    private $messageBag;

    public function __construct(MessageBag $messageBag)
    {
        $this->messageBag = $messageBag;
    }

    /**
     * Display all categories.
     *
     * @return Response
     */
    public function indexAdmin()
    {
        return view('pages.categories.list', ['categories' => Category::getAll()]);
    }

    /**
     * Show the form for creating a new category.
     *
     * @return Response
     */
    public function create()
    {
        return view('pages.categories.create', ['categories' => Category::getList()]);
    }

    /**
     * Store a newly created category in storage.
     *
     * @param CreateCategoryRequest $request
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $data = $request->validated();
        $data['order'] = $data['order'] ?? Category::getHighestOrderNumber();
        $data['slug'] = $this->generate_slug($data['name']);

        $category = Category::query()->create($data);
        if (!$category) {
            $message = $this->messageBag->add('create', 'Unable to create category.');
            return redirect('admin/categories')->withErrors($message);
        }

        return redirect('admin/categories');
    }

    /**
     * Show the form for editing the specified category.
     *
     * @param int $id
     * @return Response
     */
    public function edit(Int $id)
    {
        $data = [
            'category' => Category::query()->findOrFail($id ?? 0)
        ];

        return view('pages.categories.edit', $data);
    }

    /**
     * Update category in database.
     *
     * @param UpdateCategoryRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        if ($id !== $request->get('id')) {
            $message = $this->messageBag->add('update', 'Category id does not match');
            return redirect('admin/categories/edit/' . $id)->withErrors($message);
        }

        $item = Category::query()->findOrFail($id);

        $ignore_fields = ['id', '_token', 'creator_id'];
        foreach ($request->validated() as $key => $val) {
            if (!in_array($key, $ignore_fields)) {
                $item->$key = $val;
            }
        }

        if (!$item->save()) {
            $message = $this->messageBag->add('update', 'Unable to update resource: ' . $id);
            return redirect('admin/categories/edit/' . $id)->withErrors($message);
        }

        return redirect('admin/categories');
    }

    /**
     * Remove the category from database.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy(Int $id)
    {
        $item = Category::query()->findOrFail($id ?? 0);
        if (!$item->delete()) {
            $message = $this->messageBag->add('delete', 'Unable to delete category.');
            return redirect('admin/categories')->withErrors($message);
        }

        return redirect('admin/categories');
    }

    /**
     * Replace all non alphanumeric characters with underscore
     *
     * @param String $slug - text to clean
     * @return string
     */
    private function generate_slug(String $slug) : string
    {
        $clean = Str::slug($slug);
        return substr($clean, 0, 30);
    }
}
