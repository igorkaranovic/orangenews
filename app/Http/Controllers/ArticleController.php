<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Http\Requests\CreateArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\MessageBag;

class ArticleController extends Controller
{
    private $messageBag;

    public function __construct(MessageBag $messageBag)
    {
        $this->messageBag = $messageBag;
    }

    /**
     * Display all news articles in admin panel.
     *
     * @return Response
     */
    public function index()
    {
        return view('pages.articles.list', ['articles' => Article::getAll()]);
    }

    /**
     * Show the form for creating a new article.
     *
     * @return Response
     */
    public function create()
    {
        return view('pages.articles.create', ['categories' => Category::getList()]);
    }

    /**
     * Store a newly created article in storage.
     *
     * @param CreateArticleRequest $request
     * @return Response
     */
    public function store(CreateArticleRequest $request)
    {
        $data = $request->validated();
        $data = $this->convertTime($data);
        $data['active'] = $request->get('active', 0);

        $article = Article::query()->create($data);
        if (!$article) {
            $message = $this->messageBag->add('create','Unable to create article.');
            return redirect('admin/articles')->withErrors($message);
        }

        return redirect('admin/articles');
    }

    /**
     * Display the specified article.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $article = Article::query()->findOrFail($id);
        return view('pages.articles.view', ['article' => $article]);
    }

    /**
     * Show the form for editing the specified article.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = [
            'article' => Article::query()->findOrFail($id ?? 0),
            'categories' => Category::getList()
        ];

        return view('pages.articles.edit', $data);
    }

    /**
     * Update article in database.
     *
     * @param UpdateArticleRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateArticleRequest $request, $id)
    {
        if ($id !== $request->get('id')) {
            $message = $this->messageBag->add('update', 'Article id does not match');
            return redirect('admin/articles/edit/' . $id)->withErrors($message);
        }

        $item = Article::query()->findOrFail($id);
        $data = $request->validated();
        $data = $this->convertTime($data);

        $ignore_fields = ['id', '_token', 'creator_id'];
        foreach ($data as $key => $val) {
            if (!in_array($key, $ignore_fields)) {
                $item->$key = $val;
            }
        }

        if (!$item->save()) {
            $message = $this->messageBag->add('update','Unable to update resource: ' . $id);
            return redirect('admin/articles/edit/' . $id)->withErrors($message);
        }

        return redirect('admin/articles');
    }

    /**
     * Remove the article from database.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        $item = Article::query()->findOrFail($id ?? 0);

        if (!$item->delete()) {
            $message = $this->messageBag->add('delete','Unable to delete article.');
            return redirect('admin/articles')->withErrors($message);
        }

        return redirect('admin/articles');
    }

    /**
     * Prepare date to database
     *
     * @param array $data
     * @return array
     */
    private function convertTime(array $data) : array
    {
        $data['datetime'] = Carbon::createFromFormat('Y-m-d\TH:i', $data['datetime']);
        return $data;
    }
}
