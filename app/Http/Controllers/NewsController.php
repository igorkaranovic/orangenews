<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Response;

class NewsController extends Controller
{

    /**
     * Display all active articles.
     */
    public function index()
    {
        $data = [
            'categories' => Category::getPublic(),
            'articles' => Article::getActive(1),
            'featured' => Article::getLatest()
        ];

        return view('pages.homepage.index', $data);
    }

    /**
     * Display single articles.
     * @param $id
     * @return response
     */
    public function show($id)
    {
        $article = Article::query()->findOrFail($id ?? 0);
        $article->increment('views', 1);

        $data = [
            'categories' => Category::getPublic(),
            'article' => $article,
            'title' => $article->headline,
            'category_slug' => $article->category->slug
        ];

        return view('pages.homepage.single', $data);
    }

    /**
     * Display all active articles of specific category
     *
     * @param String $slug - category slug
     * @return Response
     */
    public function showCategory($slug)
    {
        $category = Category::getBySlug($slug);
        $articles = $category->active_articles();

        $data = [
            'categories' => Category::getPublic(),
            'articles' => $articles,
            'featured' => $articles->shift(),
            'title' => $category->name,
            'category_slug' => $slug,
        ];

        return view('pages.homepage.index', $data );
    }
}
