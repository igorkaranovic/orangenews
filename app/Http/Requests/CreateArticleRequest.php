<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        $this->request->add(['creator_id' => $user->id]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'headline'      => 'bail|required|min:2|max:200',
            'description'   => 'bail|required|min:1|max:255',
            'content'       => 'bail|required|min:1|max:30000',

            'datetime'      => 'bail|required|date',
            'active'        => 'bail|integer',

            'creator_id'    => 'bail|required|integer|exists:users,id',
            'category_id'   => 'bail|required|integer|exists:categories,id',
            ];
    }
}
