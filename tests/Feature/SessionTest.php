<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SessionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Load login page
     */
    public function test_get_login_page()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
    }

    /**
     * Try to access admin page without session, redirect to login page expected
     */
    public function test_try_access_admin_panel_without_session()
    {
        $response = $this->get('/admin');
        $response->assertRedirect('/login')->assertStatus(302);
    }

    /**
     * Access admin page with session
     */
    public function test_try_access_admin_panel_with_session() {
        $user = factory(User::class)->make();
        $response = $this->actingAs($user)->get('/admin');

        $response->assertStatus(200);
    }

    /**
     * Try to login with session, redirect to /admin expected
     */
    public function test_try_access_login_with_session() {
        $user = factory(User::class)->make();
        $response = $this->actingAs($user)->get('/login');

        $response->assertRedirect('/admin');
    }

    /**
     * Test logout
     */
    public function test_logout() {
        $user = factory(User::class)->make();
        $response = $this->actingAs($user)->get('/logout');

        $response->assertRedirect('/');
    }
}
