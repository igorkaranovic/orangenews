<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoriesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Create new category
     */
    public function test_create_category()
    {
        $data = $this->categoryData();
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->post('/admin/categories/create', $data);
        $response->assertRedirect('/admin/categories/')
            ->assertStatus(302);
    }

    /**
     * Create new category without name
     */
    public function test_try_create_category_without_name()
    {
        $user = factory(User::class)->create();
        $data = $this->categoryData();
        unset($data['name']);

        $response = $this->actingAs($user)->post('/admin/categories/create', $data);
        $response->assertSessionHasErrors('name');
        $response->assertStatus(302);
    }

    /**
     * Return fake data for categories testing
     *
     * @return array
     */
    private function categoryData() : array {
        return  [
            'name' => 'Category name',
            'slug' => 'category_name',
            'order' => 1
        ];
    }
}
