<?php

namespace Tests\Feature;

use App\Category;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ArticlesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Create new article
     */
    public function test_create_article()
    {
        $user = factory(User::class)->create();
        $category = factory(Category::class)->create();

        // create new article
        $articleData = $this->articleData($user->id, $category->id);

        $response = $this->actingAs($user)->post('/admin/articles/create', $articleData);
        $response->assertRedirect('/admin/articles/')
            ->assertStatus(302);
    }

    /**
     * Create new article with wrong date
     */
    public function test_try_create_article_with_invalid_date()
    {
        $category = factory(Category::class)->create();
        $user = factory(User::class)->create();

        $articleData = $this->articleData($user->id, $category->id);

        // set invalid date
        $articleData['datetime'] = '31.02.2020';

        $response = $this->actingAs($user)->post('/admin/articles/create', $articleData);
        $response->assertSessionHasErrors('datetime');
        $response->assertStatus(302);
    }

    /**
     * Create new article without description
     */
    public function test_try_create_article_without_description()
    {
        $category = factory(Category::class)->create();
        $user = factory(User::class)->create();

        $articleData = $this->articleData($user->id, $category->id);
        unset($articleData['description']);

        $response = $this->actingAs($user)->post('/admin/articles/create', $articleData);
        $response->assertSessionHasErrors('description');
        $response->assertStatus(302);
    }

    /**
     * Create new article without session
     */
    public function test_try_create_article_without_session()
    {
        $category = factory(Category::class)->create();
        $user = factory(User::class)->create();

        $articleData = $this->articleData($user->id, $category->id);

        $response = $this->post('/admin/articles/create', $articleData);
        $response->assertRedirect('/login')->assertStatus(302);
    }

    /**
     * Return fake data for articles testing
     *
     * @param Int $creator_id
     * @param Int $category_id
     * @return array
     */
    private function articleData(Int $creator_id, Int $category_id) : array {
        return  [
            'creator_id' => $creator_id,
            'category_id' => $category_id,
            'headline' => 'Article title',
            'description' => 'Test description',
            'content' => 'Test article content',
            'active' => 1,
            'datetime' => Carbon::now(),
        ];
    }
}
