<?php

namespace Tests\Feature;

use App\Article;
use App\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NewsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Load front page
     */
    public function test_get_home_page()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /**
     * Load login page
     */
    public function test_get_login_page()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
    }

    /**
     * Load existing category page
     */
    public function test_load_existing_category_page()
    {
        $category = factory(Category::class)->create();

        $response = $this->get('/cat/' . $category->slug);
        $response->assertStatus(200);
    }

    /**
     * Load not existing category page
     */
    public function test_load_invalid_category_page()
    {
        $response = $this->get('/cat/random-not-existing-category-name');
        $response->assertStatus(404);
    }

    /**
     * Load non existing article
     */
    public function test_load_invalid_article()
    {
        $response = $this->get('/show/22');
        $response->assertStatus(404);
    }

    /**
     * Load existing article
     */
    public function test_load_existing_article()
    {
        $article = factory(Article::class)->create();

        $response = $this->get('/news/' . $article->id);
        $response->assertStatus(200);
    }
}
