<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'NewsController@index');
Route::get('news/{id}', 'NewsController@show')->where('id', '[0-9]+');
Route::get('/cat/{category_slug}', 'NewsController@showCategory')->where('category_slug', '[\w\-]+');

Route::get('login', 'SessionController@login')->name('login');
Route::post('login', 'SessionController@authenticate');
Route::get('logout', 'SessionController@logout');

Route::group(['prefix' => 'admin', 'middleware' => ['auth:web']], function () {
    Route::get('/', 'ArticleController@index');

    Route::group(['prefix' => 'articles'], function () {
        Route::get('/', 'ArticleController@index');
        Route::get('{id}', 'ArticleController@show')->where('id', '[0-9]+');
        Route::get('create', 'ArticleController@create');
        Route::post('create', 'ArticleController@store');
        Route::get('edit/{id}', 'ArticleController@edit')->where('id', '[\d]+');
        Route::post('edit/{id}', 'ArticleController@update')->where('id', '[\d]+');
        Route::get('delete/{id}', 'ArticleController@destroy')->where('id', '[\d]+');
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', 'CategoryController@indexAdmin');
        Route::get('create', 'CategoryController@create');
        Route::post('create', 'CategoryController@store');
        Route::get('edit/{id}', 'CategoryController@edit')->where('id', '[\d]+');
        Route::post('edit/{id}', 'CategoryController@update')->where('id', '[\d]+');
        Route::get('delete/{id}', 'CategoryController@destroy')->where('id', '[\d]+');
    });
});


